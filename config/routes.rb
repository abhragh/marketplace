Rails.application.routes.draw do
  # devise_for :users
  devise_for :users, :controllers => {:registrations => "registrations"}
  get 'home/index'

  get 'home/buyers' => "home#buyers"

  get 'home/sellers' => "home#sellers"

  get 'home/all_products' => "home#all_products"

  get 'home/sold_products' => "home#sold_products"  

  root :to => "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

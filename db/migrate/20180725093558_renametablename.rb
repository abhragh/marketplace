class Renametablename < ActiveRecord::Migration[5.1]
  def self.up
    rename_table :rolls, :roles
  end

  def self.down
    rename_table :roles, :rolls
  end
end

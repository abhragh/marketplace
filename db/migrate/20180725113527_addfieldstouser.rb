class Addfieldstouser < ActiveRecord::Migration[5.1]
  def self.up  
    add_column :users, :name, :string  
    add_column :users, :address, :text
    add_column :users, :company_name, :string  
  end  

  def self.down   
  end  
end

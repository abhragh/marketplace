class AddContactNumberToUser < ActiveRecord::Migration[5.1]
  def self.up  
    add_column :users, :contact_number, :string  
  end  

  def self.down   
  end  
end
